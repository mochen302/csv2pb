# Generated from .\csv_value.g4 by ANTLR 4.9.3
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
    from typing import TextIO
else:
    from typing.io import TextIO



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\r")
        buf.write("\u0088\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\3\2")
        buf.write("\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n")
        buf.write("\3\n\7\nC\n\n\f\n\16\nF\13\n\3\n\3\n\3\13\3\13\3\13\5")
        buf.write("\13M\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\16\5\16X\n")
        buf.write("\16\3\16\3\16\3\16\3\16\5\16^\n\16\3\16\5\16a\n\16\3\16")
        buf.write("\3\16\3\16\3\16\5\16g\n\16\3\16\5\16j\n\16\3\17\3\17\3")
        buf.write("\17\7\17o\n\17\f\17\16\17r\13\17\5\17t\n\17\3\20\7\20")
        buf.write("w\n\20\f\20\16\20z\13\20\3\21\3\21\5\21~\n\21\3\21\3\21")
        buf.write("\3\22\6\22\u0083\n\22\r\22\16\22\u0084\3\22\3\22\2\2\23")
        buf.write("\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\2\27\2\31")
        buf.write("\2\33\f\35\2\37\2!\2#\r\3\2\t\4\2$$^^\n\2$$\61\61^^dd")
        buf.write("hhppttvv\5\2\62;CHch\3\2\62;\4\2GGgg\4\2--//\5\2\13\f")
        buf.write("\17\17\"\"\2\u008f\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2")
        buf.write("\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21")
        buf.write("\3\2\2\2\2\23\3\2\2\2\2\33\3\2\2\2\2#\3\2\2\2\3%\3\2\2")
        buf.write("\2\5\'\3\2\2\2\7)\3\2\2\2\t+\3\2\2\2\13\60\3\2\2\2\r\66")
        buf.write("\3\2\2\2\17;\3\2\2\2\21=\3\2\2\2\23?\3\2\2\2\25I\3\2\2")
        buf.write("\2\27N\3\2\2\2\31T\3\2\2\2\33i\3\2\2\2\35s\3\2\2\2\37")
        buf.write("x\3\2\2\2!{\3\2\2\2#\u0082\3\2\2\2%&\7=\2\2&\4\3\2\2\2")
        buf.write("\'(\7\177\2\2(\6\3\2\2\2)*\7_\2\2*\b\3\2\2\2+,\7v\2\2")
        buf.write(",-\7t\2\2-.\7w\2\2./\7g\2\2/\n\3\2\2\2\60\61\7h\2\2\61")
        buf.write("\62\7c\2\2\62\63\7n\2\2\63\64\7u\2\2\64\65\7g\2\2\65\f")
        buf.write("\3\2\2\2\66\67\7p\2\2\678\7w\2\289\7n\2\29:\7n\2\2:\16")
        buf.write("\3\2\2\2;<\7}\2\2<\20\3\2\2\2=>\7]\2\2>\22\3\2\2\2?D\7")
        buf.write("$\2\2@C\5\25\13\2AC\n\2\2\2B@\3\2\2\2BA\3\2\2\2CF\3\2")
        buf.write("\2\2DB\3\2\2\2DE\3\2\2\2EG\3\2\2\2FD\3\2\2\2GH\7$\2\2")
        buf.write("H\24\3\2\2\2IL\7^\2\2JM\t\3\2\2KM\5\27\f\2LJ\3\2\2\2L")
        buf.write("K\3\2\2\2M\26\3\2\2\2NO\7w\2\2OP\5\31\r\2PQ\5\31\r\2Q")
        buf.write("R\5\31\r\2RS\5\31\r\2S\30\3\2\2\2TU\t\4\2\2U\32\3\2\2")
        buf.write("\2VX\7/\2\2WV\3\2\2\2WX\3\2\2\2XY\3\2\2\2YZ\5\35\17\2")
        buf.write("Z[\7\60\2\2[]\5\37\20\2\\^\5!\21\2]\\\3\2\2\2]^\3\2\2")
        buf.write("\2^j\3\2\2\2_a\7/\2\2`_\3\2\2\2`a\3\2\2\2ab\3\2\2\2bc")
        buf.write("\5\35\17\2cd\5!\21\2dj\3\2\2\2eg\7/\2\2fe\3\2\2\2fg\3")
        buf.write("\2\2\2gh\3\2\2\2hj\5\35\17\2iW\3\2\2\2i`\3\2\2\2if\3\2")
        buf.write("\2\2j\34\3\2\2\2kt\7\62\2\2lp\4\63;\2mo\4\62;\2nm\3\2")
        buf.write("\2\2or\3\2\2\2pn\3\2\2\2pq\3\2\2\2qt\3\2\2\2rp\3\2\2\2")
        buf.write("sk\3\2\2\2sl\3\2\2\2t\36\3\2\2\2uw\t\5\2\2vu\3\2\2\2w")
        buf.write("z\3\2\2\2xv\3\2\2\2xy\3\2\2\2y \3\2\2\2zx\3\2\2\2{}\t")
        buf.write("\6\2\2|~\t\7\2\2}|\3\2\2\2}~\3\2\2\2~\177\3\2\2\2\177")
        buf.write("\u0080\5\35\17\2\u0080\"\3\2\2\2\u0081\u0083\t\b\2\2\u0082")
        buf.write("\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0082\3\2\2\2")
        buf.write("\u0084\u0085\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\b")
        buf.write("\22\2\2\u0087$\3\2\2\2\20\2BDLW]`fipsx}\u0084\3\b\2\2")
        return buf.getvalue()


class csv_valueLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    T__4 = 5
    T__5 = 6
    LCURLY = 7
    LBRACK = 8
    STRING = 9
    NUMBER = 10
    WS = 11

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "';'", "'}'", "']'", "'true'", "'false'", "'null'", "'{'", "'['" ]

    symbolicNames = [ "<INVALID>",
            "LCURLY", "LBRACK", "STRING", "NUMBER", "WS" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "LCURLY", 
                  "LBRACK", "STRING", "ESC", "UNICODE", "HEX", "NUMBER", 
                  "INT", "DEC", "EXP", "WS" ]

    grammarFileName = "csv_value.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.3")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


