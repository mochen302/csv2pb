# Generated from .\csv_value.g4 by ANTLR 4.9.3
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .csv_valueParser import csv_valueParser
else:
    from csv_valueParser import csv_valueParser

# This class defines a complete listener for a parse tree produced by csv_valueParser.
class csv_valueListener(ParseTreeListener):

    # Enter a parse tree produced by csv_valueParser#csv_value.
    def enterCsv_value(self, ctx:csv_valueParser.Csv_valueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#csv_value.
    def exitCsv_value(self, ctx:csv_valueParser.Csv_valueContext):
        pass


    # Enter a parse tree produced by csv_valueParser#obj.
    def enterObj(self, ctx:csv_valueParser.ObjContext):
        pass

    # Exit a parse tree produced by csv_valueParser#obj.
    def exitObj(self, ctx:csv_valueParser.ObjContext):
        pass


    # Enter a parse tree produced by csv_valueParser#array.
    def enterArray(self, ctx:csv_valueParser.ArrayContext):
        pass

    # Exit a parse tree produced by csv_valueParser#array.
    def exitArray(self, ctx:csv_valueParser.ArrayContext):
        pass


    # Enter a parse tree produced by csv_valueParser#StringValue.
    def enterStringValue(self, ctx:csv_valueParser.StringValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#StringValue.
    def exitStringValue(self, ctx:csv_valueParser.StringValueContext):
        pass


    # Enter a parse tree produced by csv_valueParser#NumberValue.
    def enterNumberValue(self, ctx:csv_valueParser.NumberValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#NumberValue.
    def exitNumberValue(self, ctx:csv_valueParser.NumberValueContext):
        pass


    # Enter a parse tree produced by csv_valueParser#ObjectValue.
    def enterObjectValue(self, ctx:csv_valueParser.ObjectValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#ObjectValue.
    def exitObjectValue(self, ctx:csv_valueParser.ObjectValueContext):
        pass


    # Enter a parse tree produced by csv_valueParser#ArrayValue.
    def enterArrayValue(self, ctx:csv_valueParser.ArrayValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#ArrayValue.
    def exitArrayValue(self, ctx:csv_valueParser.ArrayValueContext):
        pass


    # Enter a parse tree produced by csv_valueParser#BoolValue.
    def enterBoolValue(self, ctx:csv_valueParser.BoolValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#BoolValue.
    def exitBoolValue(self, ctx:csv_valueParser.BoolValueContext):
        pass


    # Enter a parse tree produced by csv_valueParser#NullValue.
    def enterNullValue(self, ctx:csv_valueParser.NullValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#NullValue.
    def exitNullValue(self, ctx:csv_valueParser.NullValueContext):
        pass



del csv_valueParser