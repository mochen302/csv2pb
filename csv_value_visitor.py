from antlr4 import *
from antlr4.InputStream import InputStream

from csv_valueLexer import csv_valueLexer
from csv_valueParser import csv_valueParser
from csv_valueVisitor import csv_valueVisitor


class CsvValueVisitor(csv_valueVisitor):
    def __init__(self):
        super().__init__()
        self.props = {}

    # Visit a parse tree produced by csv_valueParser#csv_value.
    def visitCsv_value(self, ctx: csv_valueParser.Csv_valueContext):
        print("visitCsv_value->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#AnObject.
    def visitAnObject(self, ctx: csv_valueParser.AnObjectContext):
        print("visitAnObject->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#EmptyObject.
    def visitEmptyObject(self, ctx: csv_valueParser.EmptyObjectContext):
        print("visitEmptyObject->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#ArrayOfValues.
    def visitArrayOfValues(self, ctx: csv_valueParser.ArrayOfValuesContext):
        print("visitArrayOfValues->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#EmptyArray.
    def visitEmptyArray(self, ctx: csv_valueParser.EmptyArrayContext):
        print("visitEmptyArray->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#String.
    def visitString(self, ctx: csv_valueParser.StringContext):
        print("visitString->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#Atom.
    def visitAtom(self, ctx: csv_valueParser.AtomContext):
        print("visitAtom->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#ObjectValue.
    def visitObjectValue(self, ctx: csv_valueParser.ObjectValueContext):
        print("visitObjectValue->" + ctx.getText())
        return self.visitChildren(ctx)

    # Visit a parse tree produced by csv_valueParser#ArrayValue.
    def visitArrayValue(self, ctx: csv_valueParser.ArrayValueContext):
        print("visitArrayValue->" + ctx.getText())
        return self.visitChildren(ctx)


if __name__ == '__main__':
    csv_value = "[{\"afdsfs\";[1.0;2.0]};[3;4;5];6]"
    print("start parse : " + csv_value)
    input_stream = InputStream(csv_value)

    lexer = csv_valueLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = csv_valueParser(token_stream)
    tree = parser.csv_value()

    visitor = CsvValueVisitor()
    visitor.visit(tree)
