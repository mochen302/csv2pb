# -*- coding: UTF-8 -*-

import os
import re

import xlrd

# from io import StringIO
# from csv import reader as csv_reader
import excel_sheet
import setting
import type_processor
import utils


def is_excel_file(file_path):
    base_name = os.path.basename(file_path)
    if base_name.startswith('.') or base_name.startswith('~'):
        return False
    array = file_path.split(".")
    if len(array) < 2:
        return False
    if array[1] == "xls" or array[1] == "xlsx":
        return True
    return False


def process_schema(si):
    sheet = si.sheet
    titles = sheet.row_values(0)
    types = sheet.row_values(1)
    comments = sheet.row_values(2)
    defaults = sheet.row_values(3)

    for col, title in enumerate(titles):
        type_str = types[col]
        comment = comments[col]
        default = defaults[col]

        if type_str == "":
            utils.log_exit(
                'process_schema file:{file_name} lost type for:{title}'.format(file_name=si.file_name, title=title))

        if title == "":
            title = "title_gen_%d".format(col)
            utils.log_warning(
                'process_schema file:{file_name} gen title for col:{col} gen:{title}'.format(file_name=si.file_name,
                                                                                             col=col, title=title))

        si.update_field_info(title, type_str, comment, default)


def process_data(si):
    sheet = si.sheet
    for index_row in range(setting.ROW_DATA, sheet.nrows):
        row_values = sheet.row_values(index_row)
        data_row = {}
        for index_col, title in enumerate(si.titles):
            cell_val = str(row_values[index_col])
            field_info = si.field_dict[title]
            if cell_val == "":
                cell_val = str(field_info.default)
            val = parse_field_val(field_info.type_str, cell_val)
            data_row[title] = val
        si.data_list.append(data_row)


def parse_field_val(type_str, cell_val):
    processor_func = type_processor.get_by_type_str(type_str, None)
    if processor_func is not None:
        return processor_func(cell_val)

    tt = re.split('\s|<|>|,|;|:', type_str)
    while '' in tt:
        tt.remove('')

    if len(tt) == 2:
        if tt[0] == "repeated":
            processor_func = type_processor.get_by_type_str(tt[1], str)
            if cell_val == "":
                return list()
            if cell_val.find("$$$") != -1:
                return map(processor_func,cell_val.split("$$$"))
            return map(processor_func, process_repeated(cell_val))
    if len(tt) == 3:
        if tt[0] == "map":
            if cell_val == "":
                return dict()
            data_element = re.split('\s|\n|,|;|:', cell_val)
            i = iter(data_element)
            data = dict(zip(i, i))
            processor_func_4_key = type_processor.get_by_type_str(tt[1], str)
            processor_func_4_val = type_processor.get_by_type_str(tt[2], str)
            ret = dict()
            for k, v in data.items():
                ret[processor_func_4_key(k)] = processor_func_4_val(v)
            return ret
    return cell_val


def process_repeated(data):
    regex = re.compile(r"\\.|[\"',]", re.DOTALL)
    delimiter = ''
    compos = [-1]
    for match in regex.finditer(data):
        g = match.group(0)
        if delimiter == '':
            if g == ',':
                compos.append(match.start())
            elif g in "\"'":
                delimiter = g
        elif g == delimiter:
            delimiter = ''
    if delimiter:
        raise ValueError("Unterminated string in data")
    compos.append(len(data))
    return [data[compos[i] + 1:compos[i + 1]] for i in range(len(compos) - 1)]


class Excel(object):
    def __init__(self, file_path):
        self.sheet_meta = None
        self.sheet_master_dict = dict()
        self.file_path = file_path

    def process(self):
        abs_path = os.path.abspath(self.file_path)
        file_name = os.path.basename(abs_path).split('.')[0]
        utils.log_info('process excel {file_name}'.format(file_name=file_name))

        excel_book = xlrd.open_workbook(abs_path)

        # get all sheet info and find meta sheet
        for sheet_obj in excel_book.sheets():
            si = excel_sheet.SheetInfo(file_name, sheet_obj)
            if si.get_sheet_type() == excel_sheet.SheetType.Meta:
                self.sheet_meta = si
                continue
            if si.get_sheet_type() == excel_sheet.SheetType.Master:
                self.sheet_master_dict[si.name] = si
                continue
