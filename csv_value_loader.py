import sys

from antlr4 import *
from antlr4.InputStream import InputStream

from csv_valueLexer import csv_valueLexer
from csv_valueParser import csv_valueParser


class PropertyFileLoader(csv_valueParser):
    def __init__(self, input: TokenStream):
        super().__init__(input)
        self.props = {}

    def defineProperty(self, name, value):
        self.props[name.text] = value.text

    def show(self):
        for (key, value) in self.props.items():
            print(key, '=>', value)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        input_stream = FileStream(sys.argv[1])
    else:
        input_stream = InputStream(sys.stdin.read())

    lexer = csv_valueLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = PropertyFileLoader(token_stream)
    parser.top()
    parser.show()
