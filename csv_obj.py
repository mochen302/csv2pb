import csv
import os

import utils

encoding_list = ["UTF-8-sig", "GB2312", "GBK", "UTF-16", "ASCII", "ISO-8859-1", "UTF-16"]


def is_csv_file(file_path):
    return calculate_csv_file(file_path)[1] == "csv"


def calculate_csv_file(file_path):
    base_name = os.path.basename(file_path)
    array = base_name.split(".")
    return [array[0], array[1]]


def split_lines(row):
    data_list = []
    for line in row:
        # if line.find("\"") != -1:
        #     line = line.replace('"', '\\"')
        # line = str(line.encode('utf-8'))
        data_list.append(line)
    return data_list


class CsvObj(object):
    def __init__(self, file_path):
        self.file_name = calculate_csv_file(file_path)[0]
        self.file_path = file_path

        self.name_list = []
        self.desc_list = []
        self.type_list = []
        self.data_list = []

    def process(self):
        self.read_lines()

    def read_lines(self):
        data_list = []
        for encoding_temp in encoding_list:
            try:
                with open(self.file_path, "r", newline="", encoding=encoding_temp) as data_file:
                    for row_temp in csv.reader(data_file):
                        data_list.append(row_temp)
                    utils.log_info("file:{},encoding:{}".format(self.file_path, encoding_temp))
                break
            except:
                continue

        if len(data_list) == 0:
            utils.log_error("file:{} with encoding:{} not match".format(self.file_path, encoding_list))

        index = 0
        j = 0
        for row in data_list:
            is_data_line = index >= 3
            if is_data_line:
                self.data_list.append([])

            data_list = split_lines(row)
            for data in data_list:
                data = data.strip()

                if index == 0:
                    self.name_list.append(data)
                if index == 1:
                    self.desc_list.append(data)
                if index == 2:
                    self.type_list.append(data)
                if is_data_line:
                    self.data_list[j].append(data)

            if is_data_line:
                j = j + 1
            index = index + 1


if __name__ == "__main__":
    csv_obj = CsvObj("config/csv/PBCsvTestData.csv")
    csv_obj.process()
    print(csv_obj.file_name)
    print(csv_obj.name_list)
    print(csv_obj.desc_list)
    print(csv_obj.type_list)
    print(csv_obj.data_list)
