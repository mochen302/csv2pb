from antlr4 import *
from antlr4.InputStream import InputStream

import utils
from csv_valueLexer import csv_valueLexer
from csv_valueListener import csv_valueListener
from csv_valueParser import csv_valueParser


class Node:
    def __init__(self, parent):
        self.parent = parent
        self.val = []


class CsvValueListener(csv_valueListener):
    def __init__(self):
        super().__init__()
        self.node = Node(None)
        self.current = self.node

    # Enter a parse tree produced by csv_valueParser#String.
    def enterStringValue(self, ctx: csv_valueParser.StringValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#StringValue.
    def exitStringValue(self, ctx: csv_valueParser.StringValueContext):
        value = str(ctx.getText())
        value = utils.parse_str(value)
        self.current.val.append(value)
        pass

    # Enter a parse tree produced by csv_valueParser#NumberValue.
    def enterNumberValue(self, ctx: csv_valueParser.NumberValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#NumberValue.
    def exitNumberValue(self, ctx: csv_valueParser.NumberValueContext):
        number_value = ctx.getText()
        if number_value.find(".") != -1:
            number_value = float(number_value)
        else:
            number_value = int(number_value)
        self.current.val.append(number_value)
        pass

    # Enter a parse tree produced by csv_valueParser#BoolValue.
    def enterBoolValue(self, ctx: csv_valueParser.BoolValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#BoolValue.
    def exitBoolValue(self, ctx: csv_valueParser.BoolValueContext):
        self.current.val.append(1 if ctx.getText().lower() == "true" else 0)
        pass

    # Enter a parse tree produced by csv_valueParser#NullValue.
    def enterNullValue(self, ctx: csv_valueParser.NullValueContext):
        pass

    # Exit a parse tree produced by csv_valueParser#NullValue.
    def exitNullValue(self, ctx: csv_valueParser.NullValueContext):
        self.current.val.append(ctx.getText())
        pass

    # Enter a parse tree produced by csv_valueParser#ObjectValue.
    def enterObjectValue(self, ctx: csv_valueParser.ObjectValueContext):
        node = Node(self.current)
        self.current = node
        pass

    # Exit a parse tree produced by csv_valueParser#ObjectValue.
    def exitObjectValue(self, ctx: csv_valueParser.ObjectValueContext):
        self.current.parent.val.append(self.current.val)
        self.current = self.current.parent
        pass

    # Enter a parse tree produced by csv_valueParser#ArrayValue.
    def enterArrayValue(self, ctx: csv_valueParser.ArrayValueContext):
        node = Node(self.current)
        self.current = node
        pass

    # Exit a parse tree produced by csv_valueParser#ArrayValue.
    def exitArrayValue(self, ctx: csv_valueParser.ArrayValueContext):
        self.current.parent.val.append(self.current.val)
        self.current = self.current.parent
        pass


def try_parse_csv_value(value):
    input_stream = InputStream(value)

    lexer = csv_valueLexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = csv_valueParser(token_stream)
    tree = parser.csv_value()

    walker = ParseTreeWalker()
    listener = CsvValueListener()
    walker.walk(listener, tree)
    return listener.node.val


if __name__ == '__main__':
    csv_value = "[1;\"df\";3.4;[\"4.5\";\"4.7\"];{1;\"df\";3.4;[\"4.5\";\"4.7\"];{1;\"df\";3.4;[\"4.5\";\"4.7\"]};[{1;\"df\";3.4;[\"4.5\";\"4.7\"]};{1;\"df\";3.4;[\"4.5\";\"4.7\"]}]};[{1;\"df\";3.4;[\"4.5\";\"4.7\"];{1;\"df\";3.4;[\"4.5\";\"4.7\"]};[{1;\"df\";3.4;[\"4.5\";\"4.7\"]};{1;\"df\";3.4;[\"4.5\";\"4.7\"]}]};{1;\"df\";3.4;[\"4.5\";\"4.7\"];{1;\"df\";3.4;[\"4.5\";\"4.7\"]};[{1;\"df\";3.4;[\"4.5\";\"4.7\"]};{1;\"df\";3.4;[\"4.5\";\"4.7\"]}]}]]"
    print("start parse\r\n" + csv_value)
    print(try_parse_csv_value(csv_value))
