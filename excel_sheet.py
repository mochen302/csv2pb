# -*- coding: UTF-8 -*-
import re
from enum import Enum
import setting


class SheetType(Enum):
    UnKnown = 0
    Meta = 1
    Master = 2
    Sub = 3


class FieldInfo(object):
    def __init__(self, title, type_str, comment, default):
        self.title = ''
        self.title_raw = ''
        self.link_to = ''
        self.is_id_field = False
        self.to_const = False

        self.default = default
        self.type_str = type_str.strip()
        self.comment = comment.strip()
        self.parse_title(title.strip())

    def parse_title(self, title):
        self.title_raw = title.strip()

        info = title.split("@")
        self.title = info[0].strip()
        self.is_id_field = self.title_raw == "id"

        if len(info) == 2:
            if info[1].strip() == "id":
                self.is_id_field = True
            elif info[1].strip() == "const" and self.type_str == "string":
                self.to_const = True
            else:
                self.link_to = self.title_raw

    def is_repeated(self):
        return re.split('\s|<|>|,|;|:', self.type_str)[0] == "repeated"

    def is_map(self):
        return re.split('\s|<|>|,|;|:', self.type_str)[0] == "map"


class SheetInfo(object):
    def __init__(self, file_name, sheet_obj):
        self.file_name = file_name
        self.sheet = sheet_obj
        self.field_dict = dict()
        self.data_list = list()
        self.id_cols = list()
        self.titles = list()
        self.parent_sheet_field_name = ''
        self.parent_sheet_name = ''
        self.type = SheetType.UnKnown

        self.code_get_func_name = ''
        self.code_count_func_name = ''
        self.code_file_name_key = ''

        self.name = str(sheet_obj.name.encode("utf-8").decode().strip())
        info = self.name.split("@")
        if len(info) == 1:
            if setting.NAME_OF_META_SHEET == self.name:
                self.type = SheetType.Meta
            else:
                if self.name in setting.NAME_OF_MASTER_SHEET:
                    self.type = SheetType.Master
        elif len(info) == 2:
            self.type = SheetType.Sub
            self.parent_sheet_field_name = info[0]
            self.parent_sheet_name = info[1]

    def get_sheet_type(self):
        return self.type

    def update_field_info(self, title, type_str, comment, default):
        fi = FieldInfo(title, type_str, comment, default)
        self.field_dict[fi.title] = fi
        if fi.is_id_field:
            self.id_cols.append(fi)
        self.titles.append(fi.title)

    def is_dict(self):
        return len(self.id_cols) != 0

    def get_id_type_str(self):
        id_count = len(self.id_cols)
        if 0 == id_count:
            return None
        elif 1 == id_count:
            return self.id_cols[0].type_str
        return "string"

    def get_key(self, data_dict):
        if not self.is_dict():
            return None
        id_val = list()
        for fi in self.id_cols:
            id_val.append(data_dict[fi.title])

        if 1 == len(id_val):
            return id_val[0]

        return '_'.join(str(e) for e in id_val)
