#!/bin/bash
SOURCE="${BASH_SOURCE[0]}"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
cd "$DIR"

python3 ../cmd.py  gen -w . -golang_package=server/pkg/gen/rawdata -service -doc  -swagger
