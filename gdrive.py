# -*- coding: UTF-8 -*-

import os
import shutil
import subprocess
import time

import utils


# TUTORIAL: How to get rid of 403 Errors #426
# https://github.com/gdrive-org/gdrive/issues/426


def list_files(args_dict):
    # --order modifiedTime waiting new release
    cmd_str = '''{gdrive} list --query " '{gdrive_dir}' in parents"'''.format(gdrive=args_dict['gdrive'],
                                                                              gdrive_dir=args_dict['dir'])
    if args_dict['skipHeader'] or args_dict['onlyName']:
        cmd_str += ' --no-header'

    p = subprocess.Popen(cmd_str, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    out, err = p.communicate()
    if err != "":
        utils.log_error(err)

    if args_dict['onlyName']:
        files = []
        for file_str in out.splitlines():
            file_info = file_str.split()
            if len(file_info) < 2:
                continue
            files.append(file_info[1])
        files.sort()
        return "\n".join(files)
    return out


def echo_list_files(args):
    print(list_files(vars(args)))


def sync_file_excel(file_info, args_dict):
    cmd_str = '''
            {gdrive} export --force --mime application/vnd.openxmlformats-officedocument.spreadsheetml.sheet {location}''' \
        .format(gdrive=args_dict['gdrive'], location=file_info[0])

    retry_count = 5
    delay = .200
    while retry_count > 1:
        p = subprocess.Popen(cmd_str, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                             universal_newlines=True)
        out, err = p.communicate()
        file_name = file_info[1] + ".xlsx"
        if err == "":
            utils.log_info(out)
            shutil.move(file_name, os.path.join(args_dict['out_dir'], file_name))
            return
        utils.log_error(
            "sync {file} with err {err}, waiting for {delay} seconds before retry".format(file=file_info[1],
                                                                                          err=err, delay=delay))
        time.sleep(delay)
        retry_count = retry_count - 1


def sync_files(args):
    args_dict = vars(args)
    files_lines = list_files(
        dict(dir=args_dict['dir'], skipHeader=True, onlyName=False, gdrive=args_dict['gdrive'])).splitlines()
    files = []
    files_dict = dict()
    utils.check_and_mkdir(args_dict['out_dir'])
    for file_str in files_lines:
        file_info = file_str.split()
        if len(file_info) < 2:
            continue
        files.append(file_info)
        # name to file info
        files_dict[file_info[1]] = file_info

    if args_dict['all']:
        for file_info in files:
            sync_file_excel(file_info, args_dict)
    else:
        for file_name in args_dict['file']:
            if len(file_name) == 0:
                continue
            if file_name in files_dict:
                sync_file_excel(files_dict[file_name], args_dict)
            else:
                utils.log_error("file {file} not exist".format(file=file_name))
