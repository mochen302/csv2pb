# -*- coding: UTF-8 -*-
import utils


def parse_int(str_val):
    str_val = str_val.strip()
    try:
        return 0 if str_val == "" else int(float(str_val))
    except (TypeError, ValueError):
        utils.log_warning("invalid input as *int32:%s==>0" % str_val)
        return 0


def parse_long(str_val):
    try:
        return 0 if str_val == "" else int(float(str_val))
    except (TypeError, ValueError):
        utils.log_warning("invalid input as *int64: %s ==> 0" % str_val)
        return 0


def parse_float(str_val):
    try:
        return 0 if str_val == "" else float(str_val)
    except (TypeError, ValueError):
        utils.log_warning("invalid input as float/double: %s ==> 0" % str_val)
        return 0


def parse_bool(str_val):
    str_val = str_val.lower()
    if str_val == "" or str_val == "false" or str_val == "0" or str_val == "nil" or str_val == "null":
        return False
    return True


def parse_string(str_val):
    return str(str_val)


def parse_is_json(str_val):
    import json
    try:
        json.loads(str_val)
    except ValueError:
        return False
    return True


def parse_json(str_val):
    str_val = str_val.strip()
    if str_val == "":
        return ""
    import json
    if parse_is_json(str_val):
        return json.dumps(json.loads(str_val))
    utils.log_error("invalid string as json data:{}, default with:{{}}".format(str_val))
    return "{}"


type_mapping = dict(json="string")

type_to_processor = dict(string=parse_string,
                         int=parse_int,
                         int32=parse_int,
                         uint32=parse_int,
                         sint32=parse_int,
                         fixed32=parse_int,
                         sfixed32=parse_int,
                         int64=parse_long,
                         uint64=parse_long,
                         sint64=parse_long,
                         fixed64=parse_long,
                         sfixed64=parse_long,
                         double=parse_float,
                         float=parse_float,
                         bool=parse_bool,
                         json=parse_json)


def get_type_from_mapping(type_str):
    return type_mapping[type_str] if type_str in type_mapping else type_str


def get_by_type_str(type_str, default=None):
    return type_to_processor[type_str] if type_str in type_to_processor else default
