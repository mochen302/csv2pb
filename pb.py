# -*- coding: UTF-8 -*-
import json
import os
import re
import shutil
import sys
import xml.etree.ElementTree as ET
from google.protobuf.json_format import MessageToJson

import csv_processor
import gen_golang_loader
import setting
import utils
import xml_parse


def get_message_name(si):
    return si.file_name


def load_obj(python_proto_dir, python_proto, class_name):
    output_path = os.path.join(python_proto_dir, setting.PKG_NAME_PROTO)
    utils.check_and_mkdir(output_path)
    init_path = os.path.join(output_path, '__init__.py')
    if not os.path.exists(init_path):
        # touch file is not exist at all, make this folder as real package
        open(init_path, 'a').close()

    sys.path.append(python_proto_dir)

    import imp

    fp, pathname, description = imp.find_module(setting.PKG_NAME_PROTO)
    imp.load_module(setting.PKG_NAME_PROTO, fp, pathname, description)

    module_name = os.path.join(python_proto_dir, setting.PKG_NAME_PROTO, python_proto + "_pb2.py")
    module = imp.load_source(setting.PKG_NAME_PROTO + '.' + python_proto, module_name)
    class_zz = getattr(module, class_name)
    return class_zz()


def is_pb_basic_type_repeated(typestr):
    tt = re.split('\s|<|>|,|;|:', typestr)
    packed_type = ["int", "int32", "uint32", "sint32", "fixed32", "sfixed32", "int64", "uint64", "sint64",
                   "fixed64", "sfixed64", "double", "float", "bool"]
    if len(tt) == 2:
        if tt[0] == "repeated" and tt[1] in packed_type:
            return True
    return False


def gen_proto_scheme(pbname, fileinfo):
    pbpath, protofile = os.path.split(fileinfo['out'])
    pbfile = pbname
    protosubfile = protofile

    if pbpath != "data_server":
        _, protofile = os.path.split(pbpath)
    # print("======")
    # print(protofile)
    # assert(False)
    # print(pbfile)
    # print(protosubfile)

    file_path_conf = os.path.join(setting.PATH_PROTO_RAW_PROTO, protosubfile + setting.SUFFIX_PROTO_CONF + ".proto")
    # print(file_path_conf)
    schema_all = list()
    schema_all.append('syntax = "proto3";')
    schema_all.append('\n')
    schema_all.append('import "rawdata/{message}.proto";'.format(message=protofile))
    schema_all.append('package FunPlus.Common.Config;')
    schema_all.append('option go_package = "server/pkg/gen/rawdata";')
    schema_all.append('\n')

    schema_all.append('message {message} {{'.format(message=pbname + setting.SUFFIX_PROTO_CONF))
    # entity = setting.PKG_NAME_PROTO + "." + message
    # if si.is_dict():
    schema_all.append('\tmap<{key_type},{entity}> {message}s =1;'.format(key_type=fileinfo['key_type'],
                                                                         entity=pbname, message=pbname))
    # else:
    #     schema_all.append('\trepeated {entity} {message}s =1;'.format(entity=entity, message=message))

    schema_all.append('}')

    with open(file_path_conf, 'wb') as out_file:
        out_file.write('\n'.join(schema_all).encode('UTF-8'))


def gen_proto_data(pbname, fileinfo):
    assert (False)
    # todo 读取xml 去序列化
    pbpath, protofile = os.path.split(fileinfo['out'])
    pbfile = pbname
    protosubfile = protofile

    if pbpath != "data_server":
        _, protofile = os.path.split(pbpath)

    file_name_all = protosubfile + setting.SUFFIX_PROTO_CONF
    conf_pb_name = pbname + setting.SUFFIX_PROTO_CONF
    path_out_python = os.path.join(setting.PATH_OUT_TMP, "gen/python")
    conf_obj = load_obj(path_out_python, file_name_all, conf_pb_name)
    load_obj(path_out_python, protofile, pbname)
    entity_all = getattr(conf_obj, pbname + "s")

    files_path = os.path.join(setting.PATH_CONF, fileinfo['out'])
    for fname in os.listdir(files_path):
        filename = files_path + "/" + fname

        if fname.endswith('.xml'):
            tree = ET.parse(filename)
            root = tree.getroot()
            new_entity = entity_all[int(float(root.get(fileinfo['key'])))]

            for attrib in root.keys():
                val = root.get(attrib)
                setAttrToPb(new_entity, attrib, val, filename)
            for elem in root.getchildren():
                setAttrToArrPb(new_entity, elem, filename, elem.tag)

    raw_path = os.path.join(setting.PATH_OUT_LOADER_GO, "rawdata")
    utils.check_and_mkdir(raw_path)
    with open(os.path.join(raw_path, file_name_all + ".pb"), 'wb') as out_file:
        out_file.write(conf_obj.SerializeToString())

    with open(os.path.join(raw_path, file_name_all + ".json"), 'w+') as out_file:
        out_file.write(MessageToJson(conf_obj, True))


def gen_proto_slice_data(pbname, filesliceinfo):
    _, protofilename = os.path.split(filesliceinfo['proto_file'])
    pbname = filesliceinfo['elements']
    protofile = protofilename.replace(".proto", "")
    path_out_python = os.path.join(setting.PATH_OUT_TMP, "gen/python")
    conf_obj = load_obj(path_out_python, protofile, pbname)
    entity_all = getattr(conf_obj, filesliceinfo['element'])

    config_file = os.path.join(setting.PATH_CONF, filesliceinfo['file_path'])
    if config_file.endswith(".xml"):
        tree = ET.parse(config_file)
        root = tree.getroot()
        for child in root.getchildren():
            new_entity = entity_all.add()
            for attrib in child.keys():
                val = child.get(attrib)
                xml_parse.setAttrToPb(new_entity, attrib, val, config_file)
            for elem in child.getchildren():
                xml_parse.setAttrToArrPb(new_entity, elem, config_file, elem.tag)

    elif config_file.endswith(".csv"):
        csv_processor.process(config_file, entity_all)

    raw_path = os.path.join(setting.PATH_OUT_LOADER_GO, "rawdata")
    utils.check_and_mkdir(raw_path)
    with open(os.path.join(raw_path, pbname + ".pb"), 'wb') as out_file:
        out_file.write(conf_obj.SerializeToString())

    with open(os.path.join(raw_path, pbname + ".json"), 'w+') as out_file:
        out_file.write(MessageToJson(conf_obj, True))


def protoc_proto(path,
                 gen_doc=False,

                 gen_service=False,
                 service_package=["service"],

                 gen_swagger=False):
    _protoc_proto(path, gen_doc, gen_service, service_package, gen_swagger)
    _protoc_proto(setting.PATH_ABS_PROTO_INC, gen_doc, gen_service, service_package, gen_swagger)


def protoc_proto_tmp():
    genpath = os.path.join(setting.PATH_OUT_TMP, "gen/golang")

    utils.check_and_mkdir(genpath)

    # utils.check_and_mkdir(setting.PATH_IN_CLIENT_PROTO)
    # utils.copy_files_wildcards(os.path.join(setting.PATH_CLIENT_PROTO, "*"), setting.PATH_IN_CLIENT_PROTO)
    # for fname in os.listdir(setting.PATH_IN_CLIENT_PROTO):
    #         if fname.endswith('.proto'):
    #             filepath = os.path.join(setting.PATH_IN_CLIENT_PROTO,fname)
    #             with open(filepath, 'r', encoding="utf-8") as r_file_handle:
    #                 contentarr = r_file_handle.readlines()
    #                 contentarr.insert(2, "option go_package = \"server/pkg/gen/msg\";\n")
    #                 with open(filepath, 'w', encoding="utf-8") as w_file_handle:
    #                     w_file_handle.writelines(contentarr)

    # client_proto_include = '-I {client_protopath}'.format(client_protopath = setting.PATH_IN_CLIENT_PROTO)
    # msg_proto_include = '-I {client_protopath} -I {msg_protopath}'.format(client_protopath = setting.PATH_IN_CLIENT_PROTO, msg_protopath = setting.PATH_IN_MSG_PROTO)
    msg_proto_include = '-I {msg_protopath}'.format(msg_protopath=setting.PATH_IN_MSG_PROTO)
    # server_proto_include = '-I {client_protopath} -I {msg_protopath} -I {service_protopath}'.format(client_protopath = setting.PATH_IN_CLIENT_PROTO,
    #                                                                                            msg_protopath = setting.PATH_IN_MSG_PROTO,
    #                                                                                            service_protopath = setting.PATH_IN_SERVICE_PROTO)
    server_proto_include = '-I {msg_protopath} -I {service_protopath}'.format(msg_protopath=setting.PATH_IN_MSG_PROTO,
                                                                              service_protopath=setting.PATH_IN_SERVICE_PROTO)

    # _protoc_proto_tmp(setting.PATH_IN_CLIENT_PROTO, genpath, client_proto_include,False)
    _protoc_proto_tmp(setting.PATH_IN_MSG_PROTO, genpath, msg_proto_include, False)
    _protoc_proto_tmp(setting.PATH_IN_SERVICE_PROTO, genpath, server_proto_include, True)


def _protoc_proto_tmp(path,
                      outpath,
                      includestr,
                      gen_service=False):
    # print(path)
    if not os.path.exists(path):
        assert (False)
        return
    if not path.endswith("/"):
        path += "/"

    protos = os.path.join(path, "*.proto")
    cmd = '{protoc} --plugin=protoc-gen-go={protoc_gen_go} --go_out={go_out} -I {proto_include} {includestr} ' \
          ' {protos}' \
        .format(protoc=setting.BIN_PROTOC, go_out=outpath, includestr=includestr,
                proto_include=setting.PATH_ABS_PROTO_INC,
                protos=protos, protoc_gen_go=setting.BIN_PROTOC_GEN_GO)
    # print(cmd)
    if gen_service:
        cmd += ' --plugin=protoc-gen-fun={protoc_gen_fun} --fun_out={go_out} '.format(
            protoc_gen_fun=setting.BIN_PROTOC_GEN_FUN, go_out=outpath)

    # csharp_out = os.path.join(setting.PATH_OUT_CS_CODE, dir_sub_path)
    # utils.check_and_mkdir(csharp_out)
    # cmd = '{cmd} --csharp_out={csharp_out} '.format(cmd=cmd, csharp_out=csharp_out)
    # print(cmd)
    utils.run_cmd(cmd, True, "protoc_proto")


def _protoc_proto(path,
                  gen_doc=False,

                  gen_service=False,
                  service_package=["service"],

                  gen_swagger=False):
    if not os.path.exists(path):
        return

    if not path.endswith("/"):
        path += "/"

    if gen_doc and utils.which(setting.BIN_PROTOC_GEN_DOC):
        utils.check_and_mkdir(setting.PATH_OUT_PROTO_DOC)
    else:
        gen_doc = False

    if gen_service and gen_swagger:
        utils.check_and_mkdir(setting.PATH_OUT_PROTO_SWAGGER)

    for dirpath, _, _ in os.walk(path):
        # 相对路径
        dir_sub_path = dirpath.replace(path, '').lstrip(os.sep)
        if dir_sub_path.startswith('google'):
            continue
        has_proto = False
        for fname in os.listdir(dirpath):
            if fname.endswith('.proto'):
                has_proto = True
                break
        if not has_proto:
            continue

        protos = os.path.join(dirpath, "*.proto")
        # utils.log_info("process proto path:{path} package:{package}".format(path=path, package=dir_sub_path))
        pathmsg = os.path.join(path, "msg")
        pathser = os.path.join(path, "service")
        cmd = '{protoc} --plugin=protoc-gen-go={protoc_gen_go} --go_out={go_out} -I{proto_include} -I{pathmsg} -I{pathser} ' \
              ' {protos}' \
            .format(protoc=setting.BIN_PROTOC, go_out=setting.PATH_OUT_GOLANG, proto_include=setting.PATH_ABS_PROTO_INC,
                    protos=protos, pathmsg=pathmsg, pathser=pathser, protoc_gen_go=setting.BIN_PROTOC_GEN_GO)
        # print(cmd)
        if gen_service:
            cmd += ' --plugin=protoc-gen-fun={protoc_gen_fun} --fun_out={go_out} '.format(
                protoc_gen_fun=setting.BIN_PROTOC_GEN_FUN, go_out=setting.PATH_OUT_GOLANG)

        if gen_service and gen_swagger and dir_sub_path in service_package:
            cmd += ' --plugin=protoc-gen-swagger={protoc_gen_swagger} --swagger_out=logtostderr=true:{swagger_out}'. \
                format(protoc_gen_swagger=setting.BIN_PROTOC_GEN_SWAGGER, swagger_out=setting.PATH_OUT_PROTO_SWAGGER)

        # csharp_out = os.path.join(setting.PATH_OUT_CS_CODE, dir_sub_path)
        # utils.check_and_mkdir(csharp_out)
        # cmd = '{cmd} --csharp_out={csharp_out} '.format(cmd=cmd, csharp_out=csharp_out)
        # print(cmd)
        utils.run_cmd(cmd, True, "protoc_proto")

        # # 暂通过service package确定service目录，删除客户端service代码，unity插件暂不支持annotation
        # if dir_sub_path in service_package and csharp_out != "":
        #     shutil.rmtree(csharp_out)

        if gen_doc:
            cmd = "{protoc_for_doc} --plugin=protoc-gen-doc={protoc_gen_doc} --doc_out={path_out_proto_doc} " \
                  "--doc_opt=html,{doc_name}.html -I{proto_include} -I{path} -Iprotos {protos} " \
                .format(protoc_for_doc=setting.BIN_PROTOC_FOR_GEN_DOC, protoc_gen_doc=setting.BIN_PROTOC_GEN_DOC,
                        path_out_proto_doc=setting.PATH_OUT_PROTO_DOC, doc_name=dir_sub_path, protos=protos, path=path,
                        proto_include=setting.PATH_ABS_PROTO_INC)
            utils.run_cmd(cmd, True, "protoc_proto_doc")

    js_out_file = ''
    if setting.ENABLE_JS:
        js_out_file_tmp = os.path.abspath(os.path.join(setting.PATH_OUT_JS, 'proto_temp.js'))
        js_out_file = os.path.abspath(os.path.join(setting.PATH_OUT_JS, 'proto.js'))
        cmd = '{protoc_gen_js}  --no-verify --no-convert --no-delimited --no-beautify --no-comments ' \
              '-t static-module -w commonjs -o {js_out} {protos}' \
            .format(protoc_gen_js=setting.BIN_PROTOC_GEN_JS, js_out=js_out_file_tmp,
                    protos=os.path.join(path, "*", "*.proto"))
        utils.run_cmd(cmd, True, "protos-js")
        utils.replace_js_require(js_out_file_tmp, js_out_file)

    if setting.ENABLE_TS:
        ts_out_file = os.path.abspath(os.path.join(setting.PATH_OUT_JS, 'proto.d.ts'))
        cmd = "{protoc_gen_ts} -o {ts_out_file} {proto_js}".format(protoc_gen_ts=setting.BIN_PROTOC_GEN_TS,
                                                                   ts_out_file=ts_out_file, proto_js=js_out_file)
        utils.run_cmd(cmd, True, "protos-ts")

    swagger_web_index = os.path.join(setting.PATH_ABS_TPL_SWAGGER, 'index.html')
    if gen_swagger:
        swagger_suffix = ".swagger.json"
        urls = "urls: ["
        for dirpath, _, _ in os.walk(setting.PATH_OUT_PROTO_SWAGGER):
            dir_sub_path = dirpath.replace(setting.PATH_OUT_PROTO_SWAGGER, '').lstrip(os.sep)
            if dir_sub_path == "swagger":
                continue
            for fname in os.listdir(dirpath):
                if swagger_suffix not in fname:
                    continue
                name = fname[:len(fname) - len(swagger_suffix)]
                urls += '{{name:"{name}",url:"{url}"}},'.format(name=name, url=dir_sub_path + "/" + fname)
        urls += "]"

        with open(swagger_web_index, 'r', encoding="utf-8") as in_file_handle:
            content = in_file_handle.read()
            content = content.replace('$$$urls$$$', urls)

        out_file = os.path.join(setting.PATH_OUT_PROTO_SWAGGER, 'index.html')
        with open(out_file, 'w', encoding="utf-8") as out_file_handle:
            out_file_handle.write(content)
        if not os.path.isdir(setting.PATH_OUT_PROTO_SWAGGER_WEB):
            utils.check_and_mkdir(setting.PATH_OUT_PROTO_SWAGGER_WEB)
            swagger = os.path.join(setting.PATH_ABS_TPL_SWAGGER, 'swagger')
            utils.copy_files_wildcards(os.path.join(swagger, "*"), setting.PATH_OUT_PROTO_SWAGGER_WEB)


def remove_proto_tmp():
    # utils.log_info("remove_proto_tmp had changed")
    if os.path.exists(setting.PATH_TMP):
        shutil.rmtree(setting.PATH_TMP)


def cp_proto_to_server():
    if os.path.exists(setting.PATH_OUT_SERVER):
        shutil.rmtree(setting.PATH_OUT_SERVER)
    utils.check_and_mkdir(setting.PATH_OUT_SERVER)
    resPath = os.path.join(setting.PATH_OUT_TMP, "gen/golang/server/pkg/gen")
    # print(msgsrc)
    for dirpath, _, _ in os.walk(resPath):
        if dirpath == resPath:
            continue
        dir_sub_path = dirpath.replace(resPath, '').lstrip(os.sep)
        dst = os.path.join(setting.PATH_OUT_SERVER, dir_sub_path)
        src = os.path.join(dirpath, '*.go')
        srcpb = os.path.join(dirpath, '*.pb')
        srcjson = os.path.join(dirpath, '*.json')
        utils.check_and_mkdir(dst)
        utils.copy_files_wildcards(src, dst)
        utils.copy_files_wildcards(srcpb, dst)
        utils.copy_files_wildcards(srcjson, dst)
        # utils.check_and_mkdir(dst)


def cp_rawdata_to_server():
    for suffix in ['*.pb', '*json']:
        src = os.path.join(setting.PATH_OUT_RES_PB, suffix)
        dst = os.path.join(setting.PATH_OUT_GOLANG,
                           setting.PKG_NAME_GOLANG[:setting.PKG_NAME_GOLANG.rfind("/")], 'conf', 'rawdata')
        utils.del_files_wildcards(os.path.join(dst, suffix))
        utils.copy_files_wildcards(src, dst)


def cp_rawdata_to_csharp():
    src = os.path.join(setting.PATH_OUT_RES_PB, '*.pb')
    dst = os.path.join(setting.PATH_OUT_CS, 'Conf', setting.PKG_NAME_CS)
    # remove all history data
    utils.del_files_wildcards(os.path.join(dst, "*.pb"))
    utils.del_files_wildcards(os.path.join(dst, "*.bytes"))
    utils.copy_files_wildcards(src, dst)
    # for AssetBundles
    for filename in os.listdir(dst):
        in_file_name = os.path.join(dst, filename)
        if not os.path.isfile(in_file_name):
            continue
        os.rename(in_file_name, in_file_name.replace('.pb', '.bytes'))


def gen():
    if os.path.exists(setting.PATH_OUT_TMP):
        shutil.rmtree(setting.PATH_OUT_TMP)

    genpath = os.path.join(setting.PATH_OUT_TMP, "gen/golang")

    utils.check_and_mkdir(genpath)
    utils.check_and_mkdir(setting.PATH_PROTO_RAW_PROTO)

    rawfilelist = {}
    rawslicefilelist = {}
    with open(setting.PATH_RAW_JSON_FILE, 'r') as read_file:
        content = read_file.read()
        conobj = json.loads(content)
        for pbname in conobj["xmlsheet"][0]:
            pbobj = conobj["xmlsheet"][0][pbname]
            if "data_server/" in pbobj["out"]:
                if pbobj["key_type"] == "int":
                    pbobj["key_type"] = "int32"
                if pbobj["key_type"] == "uint":
                    pbobj["key_type"] = "uint64"
                rawfilelist[pbname] = pbobj
        for pbname in conobj["xml"][0]:
            rawslicefilelist[pbname] = conobj["xml"][0][pbname]

    # print(json.dumps(rawfilelist))
    for pbname in rawfilelist:
        gen_proto_scheme(pbname, rawfilelist[pbname])

    utils.copy_files_wildcards(setting.PATH_IN_RAW_PROTO + "/*.proto", setting.PATH_PROTO_RAW_PROTO)

    for fname in os.listdir(setting.PATH_PROTO_RAW_PROTO):
        if fname.endswith('.proto'):
            filepath = os.path.join(setting.PATH_PROTO_RAW_PROTO, fname)
            with open(filepath, 'r', encoding="utf-8") as r_file_handle:
                content = r_file_handle.readlines()
                content.insert(2, "option go_package = \"server/pkg/gen/rawdata\";\n")
                with open(filepath, 'w', encoding="utf-8") as w_file_handle:
                    w_file_handle.writelines(content)
    path_out_python = os.path.join(setting.PATH_OUT_TMP, "gen/python")

    utils.check_and_mkdir(path_out_python)

    proto_path = os.path.join(setting.PATH_OUT_EXCEL_PROTO_WITH_PKG, "*.proto")
    cmd = ' {protoc} ' \
          ' -I{path_raw_proto}' \
          ' --python_out={path_out_python}' \
          ' {path_proto}' \
        .format(protoc=setting.BIN_PROTOC, path_raw_proto=setting.PATH_OUT_TMP + "/protos/",
                path_out_python=path_out_python, path_proto=setting.PATH_PROTO_RAW_PROTO + "/*.proto")
    print("generate cmd:{}".format(cmd))
    utils.run_cmd(cmd, True, "excel2pb")

    cmd = ' {protoc} ' \
          '--plugin=protoc-gen-go={protoc_gen_go} ' \
          ' -I{path_index_proto} ' \
          ' --go_out={go_out} ' \
          ' {path_proto}' \
        .format(protoc=setting.BIN_PROTOC, path_index_proto=setting.PATH_OUT_TMP + "/protos",
                protoc_gen_go=setting.BIN_PROTOC_GEN_GO,
                go_out=setting.PATH_OUT_GOLANG, path_proto=setting.PATH_PROTO_RAW_PROTO + "/*.proto")
    # print(cmd)
    utils.run_cmd(cmd, True, "excel2pb")

    # data
    for pbname in rawfilelist:
        gen_proto_data(pbname, rawfilelist[pbname])
    for pbname in rawslicefilelist:
        print("start handle:{}".format(str(pbname)))
        gen_proto_slice_data(pbname, rawslicefilelist[pbname])

    # code
    gen_golang_loader.gen_code(rawfilelist, rawslicefilelist)
