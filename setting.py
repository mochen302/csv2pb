# !/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import sys

import utils

ENABLE_JS = False
ENABLE_TS = False

# meta conf
ROW_DATA = 4
NAME_OF_META_SHEET = 'meta'
NAME_OF_MASTER_SHEET = ['Design']
SUFFIX_PROTO_CONF = "_conf"

PATH_ABS_ROOT = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
PATH_ABS_BIN = os.path.join(PATH_ABS_ROOT, 'bin')
PATH_ABS_PROTO_INC = os.path.join(PATH_ABS_ROOT, 'proto_include')
PATH_ABS_TPL = os.path.join(PATH_ABS_ROOT, 'tpl')
PATH_ABS_TPL_SWAGGER = os.path.join(PATH_ABS_TPL, 'swagger')

# conf for proto
PKG_NAME_PROTO = os.path.join(os.environ.get('PKG_NAME_PROTO', "rawdata"))

# depends current work dir
PATH_OUT = './pkg/gen'

PATH_CONF = './proto'
# PATH_CLIENT_PROTO = os.path.join(PATH_CONF, 'data_proto/xml_proto')
# PATH_IN_CLIENT_PROTO = './tmp/xml_proto'
PATH_IN_MSG_PROTO = os.path.join(PATH_CONF, 'pb_proto/common')
PATH_IN_SERVICE_PROTO = os.path.join(PATH_CONF, 'pb_proto/server')
PATH_RAW_JSON_FILE = os.path.join(PATH_CONF, 'csv_conf.json')
PATH_IN_RAW_PROTO = os.path.join(PATH_CONF, 'pb_proto/data')
PATH_OUT_TMP = './tmp'
PATH_OUT_SERVER = './pkg/gen'
PATH_PROTO_RAW_PROTO = './tmp/protos/rawdata'
PATH_PROTO_RAW_DATA = os.path.join(PATH_OUT_TMP, 'pb_gen/xml_server_proto')

# proto #######################################################################
PATH_OUT_MSG_FILE = ''
PATH_OUT_PROTO = './conf/pb_gen/pb_proto'
PATH_OUT_PROTO_CLIENT = './conf/data_proto/xml_proto'
CLIENT_INCLUDE_LIST = ['level_setting.proto',
                       'level_list.proto',
                       'match3_common.proto',
                       'hero.proto',
                       'match3_tutorials.proto',
                       'Enemy.proto']
PATH_TMP = './tmp'
PATH_OUT_SERVICE = './conf/pb_gen/pb_service'
PATH_TMP_PROTOS = os.path.join(PATH_TMP, "protos/msg")
PATH_TMP_SERVICE = os.path.join(PATH_TMP, "protos/service")

PATH_OUT_EXCEL_PROTO_WITH_PKG = os.path.join(PATH_OUT_PROTO, PKG_NAME_PROTO)
PATH_OUT_PROTO_DOC = PATH_OUT_PROTO + "_doc"
PATH_OUT_PROTO_SWAGGER = PATH_OUT_PROTO + "_swagger"
PATH_OUT_PROTO_SWAGGER_WEB = os.path.join(PATH_OUT_PROTO_SWAGGER, "swagger")

# cs ##########################################################################
PKG_NAME_CS = os.path.join(os.environ.get('PKG_NAME_CS', "rawdata"))
PATH_OUT_CS = os.path.join(PATH_OUT, 'csharp')
PATH_OUT_CS_CODE = os.path.join(PATH_OUT_CS, "code")

# golang ######################################################################
PKG_NAME_GOLANG = os.path.join(os.environ.get('PKG_NAME_GOLANG',
                                              'sstcpserver/ssnet/rawdata'))
PATH_OUT_GOLANG = os.path.join(PATH_TMP, 'gen/golang')

PATH_TMP_SERVER_RES = os.path.join(PATH_OUT_GOLANG, "server/pkg/gen")
PKG_NAME_LOADER_GO = "conf"
PATH_OUT_LOADER_GO = os.path.join(PATH_OUT_GOLANG,
                                  PKG_NAME_GOLANG[:PKG_NAME_GOLANG.rfind("/")],
                                  PKG_NAME_LOADER_GO)

# js/ts #######################################################################
PATH_OUT_JS = os.path.join(PATH_OUT, 'js')

# data ########################################################################
PATH_OUT_RES_JSON = PATH_OUT_RES_PB = os.path.join(PATH_OUT, 'rawdata')

# python ######################################################################
PATH_OUT_PYTHON = os.path.join(PATH_OUT, 'python')

path = 'oxs'
protoc = 'protoc-3.0.0-osx-x86_64'
protoc_for_gen_doc = 'protoc-3.7.1-osx-x86_64'
protoc_gen_doc = 'protoc-gen-doc'
protoc_gen_go = 'protoc-gen-go'
protoc_gen_fun = 'protoc-gen-fun'
protoc_gen_swagger = 'protoc-gen-swagger'
gdrive = "gdrive"

if sys.platform == "linux" or sys.platform == "linux2":
    path = 'linux'
    protoc = 'protoc-3.0.0-linux-x86_64'
    protoc_for_gen_doc = 'protoc-3.7.1-linux-x86_64'
elif sys.platform == 'win32':
    path = 'win'
    protoc = 'protoc-3.0.0-windows-x86_64.exe'
    protoc_for_gen_doc = 'protoc-3.7.1-windows-x86_64.exe'
elif sys.platform == 'darwin':
    path = 'osx'
else:
    utils.log_exit("unknown platform")

BIN_PROTOC = os.path.join(PATH_ABS_BIN, path, protoc)
BIN_PROTOC_FOR_GEN_DOC = os.path.join(PATH_ABS_BIN, path, protoc_for_gen_doc)
BIN_PROTOC_GEN_DOC = os.path.join(PATH_ABS_BIN, path, protoc_gen_doc)
BIN_PROTOC_GEN_GO = os.path.join(PATH_ABS_BIN, path, protoc_gen_go)
BIN_PROTOC_GEN_JS = os.path.join(PATH_ABS_BIN, 'protobufjs', 'bin', 'pbjs')
BIN_PROTOC_GEN_TS = os.path.join(PATH_ABS_BIN, 'protobufjs', 'bin', 'pbts')
BIN_PROTOC_GEN_FUN = os.path.join(PATH_ABS_BIN, path, protoc_gen_fun)
BIN_PROTOC_GEN_SWAGGER = os.path.join(PATH_ABS_BIN, path, protoc_gen_swagger)
BIN_GDRIVE = os.path.join(PATH_ABS_BIN, path, gdrive)


def exit_if_not_runnable(p):
    if not utils.which(p):
        utils.log_exit("p not find")


def update():
    sys.path.insert(0, os.path.join(PATH_ABS_BIN, path))
    [utils.check_and_mkdir(p) for p in [
        PATH_OUT,
        PATH_OUT_PROTO,
        PATH_OUT_EXCEL_PROTO_WITH_PKG,
        PATH_OUT_CS,
        PATH_OUT_CS_CODE,
        PATH_OUT_GOLANG,
        PATH_OUT_LOADER_GO,
        PATH_OUT_RES_PB,
        PATH_OUT_PYTHON,
    ]]

    [exit_if_not_runnable(p) for p in [
        BIN_PROTOC, BIN_PROTOC_GEN_GO
    ]]

    global ENABLE_JS
    if ENABLE_TS:
        ENABLE_JS = True

    if ENABLE_JS:
        [exit_if_not_runnable(p) for p in [
            BIN_PROTOC_GEN_JS
        ]]
        utils.check_and_mkdir(PATH_OUT_JS)

    if ENABLE_TS:
        [exit_if_not_runnable(p) for p in [
            BIN_PROTOC_GEN_TS
        ]]
        utils.check_and_mkdir(PATH_OUT_JS)
