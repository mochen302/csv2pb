import argparse
import os
import sys

import gdrive
import pb
import setting
import utils

dir_gdrive = '0B_4DrLzEoEL3eW51MVFJd1RXbm8'
bin_gdrive = setting.BIN_GDRIVE


def gdrive_file_list(sub_parser):
    list_parser = sub_parser.add_parser('list', help='list files under given gdrive dir')
    list_parser.add_argument('--no-header',
                             dest="skipHeader",
                             action='store_true',
                             help='skip header')
    list_parser.add_argument('--only-name',
                             dest="onlyName",
                             action='store_true',
                             help='only print file name')
    list_parser.add_argument('-g', '--gdrive',
                             type=str,
                             required=False,
                             default=bin_gdrive)

    list_parser.add_argument('-d', '--dir',
                             type=str,
                             dest="dir",
                             default=dir_gdrive,
                             help='gdrive dir hash code')
    list_parser.set_defaults(func=gdrive.echo_list_files)


def gdrive_file_sync(sub_parser):
    sync_parser = sub_parser.add_parser('sync', help='sync files under given dir')
    sync_parser.add_argument('-g', '--gdrive',
                             type=str,
                             dest="gdrive",
                             default=bin_gdrive)
    sync_parser.add_argument('-d', '--dir',
                             type=str,
                             dest="dir",
                             default=dir_gdrive,
                             help='gdrive dir hash code')
    sync_parser.add_argument('-f', '--file',
                             type=str,
                             dest="file",
                             help='sync files',
                             nargs='*')
    sync_parser.add_argument('-o', '--out',
                             type=str,
                             dest="out_dir",
                             required=True)
    sync_parser.add_argument('-a', '--all',
                             dest="all",
                             action='store_true',
                             help='sync all files under given dir')
    sync_parser.add_argument('-c', '--csv',
                             dest="csv",
                             action='store_true',
                             help='need csv files?'
                             )
    sync_parser.set_defaults(func=gdrive.sync_files)


def proto_gen(sub_parser):
    sync_parser = sub_parser.add_parser('gen', help='generate proto files, raw data and code')
    sync_parser.add_argument('-w', '--dir_work',
                             type=str,
                             dest="dir_work",
                             required=True,
                             help='work dir,should contains folder "excel" and "protos"'
                             )
    sync_parser.add_argument('-folder_excel',
                             type=str,
                             dest="folder_excel",
                             required=False,
                             default='excel',
                             help='excel folder name under work dir')
    sync_parser.add_argument('-folder_proto',
                             type=str,
                             dest="folder_proto",
                             required=False,
                             default='protos',
                             help='excel folder name under work dir')

    sync_parser.add_argument('-golang_package',
                             type=str,
                             dest="excel_golang_package",
                             default="server/pkg/gen/rawdata",
                             )
    sync_parser.add_argument('-doc',
                             dest="doc",
                             default=False,
                             action='store_true',
                             )
    sync_parser.add_argument('-_tags_',
                             dest="_tags_",
                             help='env tag',
                             nargs='*')
    sync_parser.add_argument('-service',
                             dest="service",
                             default=False,
                             action='store_true',
                             )
    sync_parser.add_argument('-swagger',
                             dest="swagger",
                             default=False,
                             action='store_true',
                             )
    sync_parser.set_defaults(func=gen_pb)


def gen_pb(args):
    args_dict = vars(args)
    os.environ['PKG_NAME_GOLANG'] = args_dict['excel_golang_package']
    dir_work = os.path.abspath(args_dict['dir_work'])

    # must change to work dir
    os.chdir(dir_work)

    # dir_excel = os.path.abspath(os.path.join(dir_work, args_dict['folder_excel']))
    # dir_proto = os.path.abspath(os.path.join(dir_work, args_dict['folder_proto']))
    utils.reload(setting)

    pb.gen()

    # pb.protoc_proto(dir_proto, args_dict['doc'], args_dict['service'], ["service"], args_dict['swagger'])
    pb.protoc_proto_tmp()
    pb.cp_proto_to_server()
    # pb.cp_rawdata_to_server()
    # pb.cp_rawdata_to_csharp()
    pb.remove_proto_tmp()


def cmd():
    parser = argparse.ArgumentParser(description='excel2pb')
    sub_parser = parser.add_subparsers(title='sub commands')
    gdrive_file_list(sub_parser)
    gdrive_file_sync(sub_parser)
    proto_gen(sub_parser)

    args = parser.parse_args()
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    if hasattr(args, 'func'):
        args.func(args)


if __name__ == "__main__":
    cmd()
