# Generated from csv_value.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .csv_valueParser import csv_valueParser
else:
    from csv_valueParser import csv_valueParser

# This class defines a complete generic visitor for a parse tree produced by csv_valueParser.

class csv_valueVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by csv_valueParser#csv_value.
    def visitCsv_value(self, ctx:csv_valueParser.Csv_valueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#obj.
    def visitObj(self, ctx:csv_valueParser.ObjContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#array.
    def visitArray(self, ctx:csv_valueParser.ArrayContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#StringValue.
    def visitStringValue(self, ctx:csv_valueParser.StringValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#NumberValue.
    def visitNumberValue(self, ctx:csv_valueParser.NumberValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#ObjectValue.
    def visitObjectValue(self, ctx:csv_valueParser.ObjectValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#ArrayValue.
    def visitArrayValue(self, ctx:csv_valueParser.ArrayValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#BoolValue.
    def visitBoolValue(self, ctx:csv_valueParser.BoolValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by csv_valueParser#NullValue.
    def visitNullValue(self, ctx:csv_valueParser.NullValueContext):
        return self.visitChildren(ctx)



del csv_valueParser