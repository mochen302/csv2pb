from google.protobuf.descriptor import FieldDescriptor

import csv_obj
import csv_value_listener
import utils

log_info_arr = []
log_err_arr = []


def log_error(info):
    if info in log_err_arr:
        return
    log_err_arr.append(info)
    utils.log_error(info)


def log_info(info):
    if info in log_info_arr:
        return
    log_info_arr.append(info)
    utils.log_info(info)


def process(config_file, entity_all):
    csv_file = csv_obj.CsvObj(config_file)
    csv_file.process()
    for value in csv_file.data_list:
        is_empty = True
        for sub_value in value:
            if sub_value.strip() != "":
                is_empty = False
                break

        if is_empty:
            continue

        i = 0
        new_entity = entity_all.add()
        for sub_value in value:
            if sub_value.strip() == "":
                i = i + 1
                continue

            name = csv_file.name_list[i]
            type = str(csv_file.type_list[i])
            if type.endswith("[]"):
                value_arr = []
                is_base = True
                for sub_sub_value in sub_value.split(";"):
                    if type.find("int") != -1:
                        value_arr.append(int(sub_sub_value))
                    elif type.find("float") != -1:
                        value_arr.append(parse_float(sub_sub_value))
                    elif type.find("bool") != -1:
                        value_arr.append(parse_bool(sub_sub_value))
                    elif type.find("string") != -1:
                        value_arr.append(str(sub_sub_value))
                    elif type.lower().startswith("enum"):
                        value_arr.append(str(sub_sub_value))
                    else:
                        is_base = False
                        break
                try:
                    if not is_base:
                        __setAttrToPb__(new_entity, name, sub_value, config_file, True, False)
                    else:
                        __setAttrToPb__(new_entity, name, value_arr, config_file, False, False)
                except ValueError as v:
                    log_error(
                        "process error:%s file:%s column:%s value:%s row_value:%s row:%s " % (
                            v, config_file, name, sub_value, value, i))
            else:
                name_index = name.find("[")
                current_is_single = False
                if name_index != -1:
                    name = name[0:name_index]
                    current_is_single = True
                try:
                    __setAttrToPb__(new_entity, name, sub_value, config_file, True, current_is_single)
                except ValueError as v:
                    log_error(
                        "process error:%s file:%s column:%s value:%s row_value:%s row:%s " % (
                            v, config_file, name, sub_value, value, i))

            i = i + 1


def __setAttrToPb__(new_entity, attrib, val, filename, need_parse, current_is_single):
    try:
        if attrib.strip() == "":
            return
        fields_info = new_entity.DESCRIPTOR.fields_by_name[attrib]
    except:
        log_info("filename:%s column:%s not define in proto" % (filename, attrib))
        return

    t = fields_info.type

    try:
        if fields_info.enum_type is not None:
            enum_str = str(val)
            if current_is_single:
                ev = fields_info.enum_type.values_by_name[enum_str]
                getattr(new_entity, attrib).append(ev.number)
            elif not try_set_list_value(new_entity, attrib, val, False, True, fields_info):
                ev = fields_info.enum_type.values_by_name[enum_str]
                setattr(new_entity, attrib, ev.number)

        elif t == FieldDescriptor.TYPE_INT32 or t == FieldDescriptor.TYPE_UINT32:
            if current_is_single:
                getattr(new_entity, attrib).append(int(val))
            elif not try_set_list_value(new_entity, attrib, val, False):
                setattr(new_entity, attrib, int(val))

        elif t == FieldDescriptor.TYPE_FLOAT:
            if current_is_single:
                getattr(new_entity, attrib).append(parse_float(val))
            elif not try_set_list_value(new_entity, attrib, val, False):
                setattr(new_entity, attrib, parse_float(val))

        elif t == FieldDescriptor.TYPE_BOOL:
            if current_is_single:
                getattr(new_entity, attrib).append(parse_bool(val))
            elif not try_set_list_value(new_entity, attrib, val, False):
                setattr(new_entity, attrib, parse_bool(val))

        elif t == FieldDescriptor.TYPE_STRING:
            if current_is_single:
                getattr(new_entity, attrib).append(str(val))
            elif not try_set_list_value(new_entity, attrib, val, True):
                val = str(val)
                setattr(new_entity, attrib, utils.parse_str(str(val)))

        elif t == FieldDescriptor.TYPE_MESSAGE:
            val_arr = val
            if need_parse:
                val = "[" + val + "]"
                val_arr = csv_value_listener.try_parse_csv_value(val)

            is_obj_list = False
            try:
                new_sub_entity = getattr(new_entity, attrib).add()
                is_obj_list = True
            except:
                new_sub_entity = getattr(new_entity, attrib)

            proto_id = 1
            for sub_value in val_arr:
                if (not current_is_single) and is_obj_list:
                    if proto_id > 1:
                        new_sub_entity = getattr(new_entity, attrib).add()
                    proto_sub_id = 1
                    for sub_sub_value in sub_value:
                        current_attrib = new_sub_entity.DESCRIPTOR.fields_by_number[proto_sub_id]
                        __setAttrToPb__(new_sub_entity, current_attrib.name, sub_sub_value, filename, False, False)
                        proto_sub_id += 1
                else:
                    current_attrib = new_sub_entity.DESCRIPTOR.fields_by_number[proto_id]
                    __setAttrToPb__(new_sub_entity, current_attrib.name, sub_value, filename, False, False)
                proto_id += 1

        else:
            print(t)
    except ValueError as err:
        log_error(
            "process error:%s file:%s column:%s value:%s row_value:%s" % (
                err, filename, attrib, val, val))
    except AttributeError as err:
        log_error(
            "process error:%s file:%s column:%s value:%s row_value:%s" % (
                err, filename, attrib, val, val))


def parse_float(val):
    try:
        cur_val = float(val)
    except:
        cur_val = float(int(val) * 1.0)
    return cur_val


def parse_bool(val):
    try:
        cur_val = True if int(val) == 1 else False
    except:
        cur_val = True if str(val).lower() == "true" else False
    return cur_val


def try_set_list_value(new_entity, attrib, val, is_str, is_enum=False, fields_info=None):
    if isinstance(val, list):
        new_sub_entity = getattr(new_entity, attrib)
        for sub_value in val:
            if is_str:
                sub_value = utils.parse_str(sub_value)
            if is_enum:
                ev = fields_info.enum_type.values_by_name[sub_value]
                sub_value = ev.number
            new_sub_entity.append(sub_value)
        return True
    else:
        return False
