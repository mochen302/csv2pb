# -*- coding: UTF-8 -*-

import errno
import glob
import hashlib
import os
import re
import shutil
import subprocess
import sys
import zipfile
from itertools import chain, combinations

CAMEL_CASE_TEST_RE = re.compile(r'^[a-zA-Z]*([a-z]+[A-Z]+|[A-Z]+[a-z]+)[a-zA-Z\d]*$')
CAMEL_CASE_REPLACE_RE = re.compile(r'([a-z]|[A-Z]+)(?=[A-Z])')
SNAKE_CASE_TEST_RE = re.compile(r'^[a-z]+([a-z\d]+_|_[a-z\d]+)+[a-z\d]+$')
SNAKE_CASE_TEST_DASH_RE = re.compile(r'^[a-z]+([a-z\d]+-|-[a-z\d]+)+[a-z\d]+$')
SNAKE_CASE_REPLACE_RE = re.compile(r'(_)([a-z\d])')
SNAKE_CASE_REPLACE_DASH_RE = re.compile('(-)([a-z\d])')


def run_cmd(cmd, silent=True, tag="no-tag"):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    if not silent:
        log_info("run_cmd {tag} {cmd}".format(tag=tag, cmd=cmd))
    for line in p.stdout.readlines():
        log_error("\t\t==> {line}".format(line=line))
    return p.wait()


def all_subsets(ss):
    return chain(*map(lambda x: combinations(ss, x), range(1, len(ss))))


def check_and_mkdir(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            return True
        else:
            return False
    return True


def is_full_string(string):
    """
    Check if a string is not empty (it must contains at least one non space character).
    :param string: String to check.
    :type string: str
    :return: True if not empty, false otherwise.
    """
    return is_string(string) and string.strip() != ''


def is_string(obj):
    try:  # basestring is available in python 2 but missing in python 3!
        return isinstance(obj, basestring)
    except NameError:
        return isinstance(obj, str)


def is_snake_case(string, separator='_'):
    if is_full_string(string):
        re_map = {
            '_': SNAKE_CASE_TEST_RE,
            '-': SNAKE_CASE_TEST_DASH_RE
        }
        re_template = '^[a-z]+([a-z\d]+{sign}|{sign}[a-z\d]+)+[a-z\d]+$'
        r = re_map.get(separator, re.compile(re_template.format(sign=re.escape(separator))))
        return bool(r.search(string))
    return False


def snake_case_to_camel(string, upper_case_first=True, separator='_'):
    if not is_string(string):
        raise TypeError('Expected string')
    re_map = {
        '_': SNAKE_CASE_REPLACE_RE,
        '-': SNAKE_CASE_REPLACE_DASH_RE
    }
    r = re_map.get(separator, re.compile('({sign})([a-z\d])'.format(sign=re.escape(separator))))
    string = r.sub(lambda m: m.group(2).upper(), string)
    if upper_case_first:
        return string[0].upper() + string[1:]
    return string


def replace_js_require(in_file, out_file):
    with open(in_file, 'r', encoding="utf-8") as in_file_handle:
        content = in_file_handle.read()
        content = content.replace('var $protobuf = require("protobufjs/minimal");',
                                  'var protobuf = require("protobuf");var $protobuf = protobuf;')
        with open(out_file, 'w', encoding="utf-8") as out_file_handle:
            out_file_handle.write(content)
    os.remove(in_file)


def which(program):
    def is_exe(file_path_in):
        return os.path.isfile(file_path_in) and os.access(file_path_in, os.X_OK)

    file_path, file_name = os.path.split(program)
    if file_path:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None


def camel_case(x):
    return ''.join([x[0].lower(), x[1:]])


def reload(lib):
    if sys.version_info[0] < 3 or (sys.version_info[0] == 3 and sys.version_info[1] < 4):
        import imp
        imp.reload(lib)
    else:
        import importlib
        importlib.reload(lib)


def log_exit(msg):
    log_error(msg)
    exit()


def log_warning(msg):
    print("[warning] " + msg + "\n")


def log_error(msg):
    print("[error] " + msg + "\n")


def log_info(msg):
    print("[info] " + msg)


def md5(file_name):
    hash_md5 = hashlib.md5()
    with open(file_name, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def zip_dir(path, zip_dest, extension):
    zip_handle = zipfile.ZipFile(zip_dest, 'w')
    # zip_handle is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(extension):
                zip_handle.write(os.path.join(root, file), file)
    zip_handle.close()


def copy_files_wildcards(src, dest):
    check_and_mkdir(dest)
    for file in glob.glob(src):
        shutil.copy(file, dest)


def del_files_wildcards(src):
    for file in glob.glob(src):
        os.remove(file)


def parse_str(str_value):
    str_value = str(str_value)
    if str_value.startswith("\""):
        str_value = str_value[1:]
    if str_value.endswith("\""):
        str_value = str_value[0:len(str_value) - 1]
    return str_value
    # if str_value.find("\"") != -1:
    #     str_value = str_value.replace("\"", "")
    # return str_value
