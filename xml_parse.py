from google.protobuf.descriptor import FieldDescriptor

import utils


def setAttrToPb(new_entity, attrib, val, filename):
    t = 1

    try:
        if new_entity is not None:
            if hasattr(new_entity, attrib):
                fieldsinfo = new_entity.DESCRIPTOR.fields_by_name[attrib]
                t = fieldsinfo.type

                if fieldsinfo.enum_type != None:
                    enumstr = str(val)
                    ev = fieldsinfo.enum_type.values_by_name[enumstr]
                    if ev != None:
                        setattr(new_entity, attrib, ev.number)
                elif fieldsinfo.type == FieldDescriptor.TYPE_INT32 or fieldsinfo.type == FieldDescriptor.TYPE_UINT32:
                    if not set_list_value(new_entity, attrib, val):
                        setattr(new_entity, attrib, int(val))

                elif fieldsinfo.type == FieldDescriptor.TYPE_FLOAT:
                    if not set_list_value(new_entity, attrib, val):
                        setattr(new_entity, attrib, float(val))

                elif fieldsinfo.type == FieldDescriptor.TYPE_BOOL:
                    if not set_list_value(new_entity, attrib, val):
                        setattr(new_entity, attrib, True if val == "1" else False)

                elif fieldsinfo.type == FieldDescriptor.TYPE_STRING:
                    if not set_list_value(new_entity, attrib, val):
                        setattr(new_entity, attrib, str(val))
                else:
                    print(fieldsinfo.type)

    except ValueError:
        utils.log_exit(
            "file:[%s], colum [%s] val[%s] t[%s] produced an error [%s]" % (filename, attrib, val, t, ValueError.msg))
    except:
        utils.log_exit("file:[%s], colum [%s] produced an error" % (filename, attrib))


def set_list_value(new_entity, attrib, val):
    if isinstance(val, list):
        new_sub_entity = getattr(new_entity, attrib)
        for sub_value in val:
            new_sub_entity.append(sub_value)
        return True
    else:
        return False


def setAttrToArrPb(new_entity, elem, filename, colum_name):
    try:
        if new_entity is not None:
            attrib = elem.tag
            val = elem.text
            if hasattr(new_entity, attrib):
                curentity = getattr(new_entity, attrib)
                fieldsinfo = new_entity.DESCRIPTOR.fields_by_name[attrib]
                if fieldsinfo.type == FieldDescriptor.TYPE_INT32 or fieldsinfo.type == FieldDescriptor.TYPE_UINT32:
                    curentity.append(int(val))
                elif fieldsinfo.type == FieldDescriptor.TYPE_MESSAGE:
                    try:
                        elem_entity = curentity.add()
                    except:
                        elem_entity = curentity

                    for attrib in elem.keys():
                        val = elem.get(attrib)
                        setAttrToPb(elem_entity, attrib, val, filename)
                    for elemattr in elem.getchildren():
                        setAttrToArrPb(elem_entity, elemattr, filename, elemattr.tag)

                elif fieldsinfo.type == FieldDescriptor.TYPE_FLOAT:
                    curentity.append(float(val))
                elif fieldsinfo.type == FieldDescriptor.TYPE_BOOL:
                    curentity.append(True if val == "1" else False)
                elif fieldsinfo.type == FieldDescriptor.TYPE_STRING:
                    curentity.append(val)
                else:
                    print(fieldsinfo.type)
    except ValueError:
        utils.log_exit("file:[%s], colum [%s] produced an error [%s]" % (filename, colum_name, ValueError.msg))
