import json
import os
import shutil
import sys
import xml.etree.ElementTree as ET

from google.protobuf.json_format import MessageToJson

import csv_processor
import setting
import utils
import xml_parse

conf = "csv_conf.json"
conf_dir = "config"
conf_temp_dir = "temp"
conf_module = "raw_data"
conf_temp_dir_module = os.path.join(conf_temp_dir, conf_module)
conf_temp_dir_proto = os.path.join(conf_temp_dir_module, "proto")
conf_temp_dir_python = os.path.join(conf_temp_dir_module, "python")
proto_common_list = ["system"]


def load_obj(conf_temp_dir, conf_temp_dir_python, python_proto, class_name):
    sys.path.append(conf_temp_dir)

    import imp

    fp, pathname, description = imp.find_module(conf_module, [conf_temp_dir])
    imp.load_module(conf_module, fp, pathname, description)

    module_name = os.path.join(conf_temp_dir_python, python_proto + "_pb2.py")
    module = imp.load_source(conf_temp_dir_python, module_name)
    class_zz = getattr(module, class_name)
    return class_zz()


def gen_proto_slice_data(file_slice_info, conf_temp_dir, conf_temp_dir_python, conf_out_dir):
    _, protofilename = os.path.split(file_slice_info['proto_file'])
    protofile = protofilename.replace(".proto", "")

    pb_name = file_slice_info["elements"]
    conf_obj = load_obj(conf_temp_dir, conf_temp_dir_python, protofile, pb_name)
    entity_all = getattr(conf_obj, file_slice_info['element'])

    config_file = file_slice_info['file_path']
    if config_file.endswith(".xml"):
        config_file = os.path.join(conf_dir, "xml", config_file)

        tree = ET.parse(config_file)
        root = tree.getroot()
        for child in list(root):
            new_entity = entity_all.add()
            for attrib in child.keys():
                val = child.get(attrib)
                xml_parse.setAttrToPb(new_entity, attrib, val, config_file)
            for elem in list(child):
                xml_parse.setAttrToArrPb(new_entity, elem, config_file, elem.tag)

    elif config_file.endswith(".csv"):
        config_file = os.path.join(conf_dir, "csv", config_file)
        csv_processor.process(config_file, entity_all)

    utils.check_and_mkdir(conf_out_dir)

    pb_config_name = str(file_slice_info['pb_file'])
    pb_config_name = pb_config_name.split(".")[0]
    with open(os.path.join(conf_out_dir, pb_config_name + ".pb"), 'wb') as out_file:
        out_file.write(conf_obj.SerializeToString())

    with open(os.path.join(conf_out_dir, pb_config_name + ".json"), 'w+') as out_file:
        out_file.write(MessageToJson(conf_obj, True))


def parse(conf, conf_temp_dir, conf_dir, conf_out_dir):
    raw_file_arr = {}
    pb_file_arr = []
    with open(conf, 'r') as read_file:
        content = read_file.read()
        conobj = json.loads(content)
        for pbname in conobj["xml"][0]:
            file_info = conobj["xml"][0][pbname]
            raw_file_arr[pbname] = file_info
            pb_file = file_info['pb_file']
            if pb_file in pb_file_arr:
                msg = "config:{} pb_file:{} has duplicate!".format(pbname, pb_file)
                utils.log_error(msg)
                raise ValueError(msg)
            pb_file_arr.append(pb_file)

    utils.check_and_mkdir(conf_temp_dir_proto)
    utils.check_and_mkdir(conf_temp_dir_python)

    utils.copy_files_wildcards(os.path.join(conf_dir, "proto", "*.proto"), conf_temp_dir_proto)

    for fname in os.listdir(conf_temp_dir_proto):
        if fname.endswith('.proto'):
            filepath = os.path.join(conf_temp_dir_proto, fname)
            with open(filepath, 'r', encoding="utf-8") as r_file_handle:
                content = r_file_handle.readlines()
                content.insert(2, "option go_package = \"server/pkg/gen/rawdata\";\n")
                with open(filepath, 'w', encoding="utf-8") as w_file_handle:
                    w_file_handle.writelines(content)

    bin_protoc = os.path.join("bin", setting.path, setting.protoc)
    print(bin_protoc)

    cmd = ' {protoc} ' \
          ' -I{path_raw_proto}' \
          ' --python_out={path_out_python}' \
          ' {path_proto}' \
        .format(protoc=bin_protoc, path_raw_proto=conf_temp_dir_proto,
                path_out_python=conf_temp_dir_python, path_proto=os.path.join(conf_temp_dir_proto, "*.proto"))
    utils.run_cmd(cmd, True, "excel2pb")

    if len(proto_common_list) > 0:
        for python_file in os.listdir(conf_temp_dir_python):
            if python_file.endswith(".py"):
                python_file = os.path.join(conf_temp_dir_python, python_file)
                with open(python_file, 'r', encoding="utf-8") as r_file_handle:
                    content_list = r_file_handle.readlines()
                    has_change = False
                    i = 0
                    for content in content_list:
                        content = content.strip()
                        for proto_common in proto_common_list:
                            key = "import " + proto_common
                            index = content.find(key, 0, len(content))
                            if index != -1:
                                content = "import temp.raw_data.python." + proto_common + str(
                                    content[index + len(key):])
                                content_list[i] = content
                                has_change = True
                        i += 1
                    if has_change:
                        with open(python_file, 'w', encoding="utf-8") as r_file_handle_now:
                            r_file_handle_now.writelines(content_list)

    for pbname in raw_file_arr:
        gen_proto_slice_data(raw_file_arr[pbname], conf_temp_dir, conf_temp_dir_python, conf_out_dir)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        proto_common_str = sys.argv[1].strip()
        for common_str in (proto_common_str.split(",")):
            if common_str not in proto_common_list:
                proto_common_list.append(common_str)

    utils.check_and_mkdir(conf_dir)

    conf_out_dir = os.path.join(conf_dir, "pb")
    if os.path.exists(conf_out_dir):
        shutil.rmtree(conf_out_dir)
    utils.check_and_mkdir(conf_out_dir)

    if os.path.exists(conf_temp_dir):
        shutil.rmtree(conf_temp_dir)

    utils.check_and_mkdir(conf_temp_dir_module)
    utils.copy_files_wildcards("__init__.py", conf_temp_dir_module)
    utils.copy_files_wildcards("__init__.py", conf_temp_dir)
    utils.copy_files_wildcards("__init__.py", conf_temp_dir_python)

    parse(conf, conf_temp_dir, conf_dir, conf_out_dir)
