# Generated from .\csv_value.g4 by ANTLR 4.9.3
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\r")
        buf.write("\66\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\3\2\5\2\r\n\2")
        buf.write("\3\3\3\3\3\3\3\3\7\3\23\n\3\f\3\16\3\26\13\3\3\3\3\3\3")
        buf.write("\3\3\3\5\3\34\n\3\3\4\3\4\3\4\3\4\7\4\"\n\4\f\4\16\4%")
        buf.write("\13\4\3\4\3\4\3\4\3\4\5\4+\n\4\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\5\5\64\n\5\3\5\2\2\6\2\4\6\b\2\2\2<\2\f\3\2\2\2\4")
        buf.write("\33\3\2\2\2\6*\3\2\2\2\b\63\3\2\2\2\n\r\5\4\3\2\13\r\5")
        buf.write("\6\4\2\f\n\3\2\2\2\f\13\3\2\2\2\r\3\3\2\2\2\16\17\7\t")
        buf.write("\2\2\17\24\5\b\5\2\20\21\7\3\2\2\21\23\5\b\5\2\22\20\3")
        buf.write("\2\2\2\23\26\3\2\2\2\24\22\3\2\2\2\24\25\3\2\2\2\25\27")
        buf.write("\3\2\2\2\26\24\3\2\2\2\27\30\7\4\2\2\30\34\3\2\2\2\31")
        buf.write("\32\7\t\2\2\32\34\7\4\2\2\33\16\3\2\2\2\33\31\3\2\2\2")
        buf.write("\34\5\3\2\2\2\35\36\7\n\2\2\36#\5\b\5\2\37 \7\3\2\2 \"")
        buf.write("\5\b\5\2!\37\3\2\2\2\"%\3\2\2\2#!\3\2\2\2#$\3\2\2\2$&")
        buf.write("\3\2\2\2%#\3\2\2\2&\'\7\5\2\2\'+\3\2\2\2()\7\n\2\2)+\7")
        buf.write("\5\2\2*\35\3\2\2\2*(\3\2\2\2+\7\3\2\2\2,\64\7\13\2\2-")
        buf.write("\64\7\f\2\2.\64\5\4\3\2/\64\5\6\4\2\60\64\7\6\2\2\61\64")
        buf.write("\7\7\2\2\62\64\7\b\2\2\63,\3\2\2\2\63-\3\2\2\2\63.\3\2")
        buf.write("\2\2\63/\3\2\2\2\63\60\3\2\2\2\63\61\3\2\2\2\63\62\3\2")
        buf.write("\2\2\64\t\3\2\2\2\b\f\24\33#*\63")
        return buf.getvalue()


class csv_valueParser ( Parser ):

    grammarFileName = "csv_value.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "';'", "'}'", "']'", "'true'", "'false'", 
                     "'null'", "'{'", "'['" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "LCURLY", "LBRACK", 
                      "STRING", "NUMBER", "WS" ]

    RULE_csv_value = 0
    RULE_obj = 1
    RULE_array = 2
    RULE_value = 3

    ruleNames =  [ "csv_value", "obj", "array", "value" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    LCURLY=7
    LBRACK=8
    STRING=9
    NUMBER=10
    WS=11

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.3")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class Csv_valueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def obj(self):
            return self.getTypedRuleContext(csv_valueParser.ObjContext,0)


        def array(self):
            return self.getTypedRuleContext(csv_valueParser.ArrayContext,0)


        def getRuleIndex(self):
            return csv_valueParser.RULE_csv_value

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCsv_value" ):
                listener.enterCsv_value(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCsv_value" ):
                listener.exitCsv_value(self)




    def csv_value(self):

        localctx = csv_valueParser.Csv_valueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_csv_value)
        try:
            self.state = 10
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [csv_valueParser.LCURLY]:
                self.enterOuterAlt(localctx, 1)
                self.state = 8
                self.obj()
                pass
            elif token in [csv_valueParser.LBRACK]:
                self.enterOuterAlt(localctx, 2)
                self.state = 9
                self.array()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ObjContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LCURLY(self):
            return self.getToken(csv_valueParser.LCURLY, 0)

        def value(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(csv_valueParser.ValueContext)
            else:
                return self.getTypedRuleContext(csv_valueParser.ValueContext,i)


        def getRuleIndex(self):
            return csv_valueParser.RULE_obj

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObj" ):
                listener.enterObj(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObj" ):
                listener.exitObj(self)




    def obj(self):

        localctx = csv_valueParser.ObjContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_obj)
        self._la = 0 # Token type
        try:
            self.state = 25
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 12
                self.match(csv_valueParser.LCURLY)
                self.state = 13
                self.value()
                self.state = 18
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==csv_valueParser.T__0:
                    self.state = 14
                    self.match(csv_valueParser.T__0)
                    self.state = 15
                    self.value()
                    self.state = 20
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 21
                self.match(csv_valueParser.T__1)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 23
                self.match(csv_valueParser.LCURLY)
                self.state = 24
                self.match(csv_valueParser.T__1)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArrayContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LBRACK(self):
            return self.getToken(csv_valueParser.LBRACK, 0)

        def value(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(csv_valueParser.ValueContext)
            else:
                return self.getTypedRuleContext(csv_valueParser.ValueContext,i)


        def getRuleIndex(self):
            return csv_valueParser.RULE_array

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArray" ):
                listener.enterArray(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArray" ):
                listener.exitArray(self)




    def array(self):

        localctx = csv_valueParser.ArrayContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_array)
        self._la = 0 # Token type
        try:
            self.state = 40
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 27
                self.match(csv_valueParser.LBRACK)
                self.state = 28
                self.value()
                self.state = 33
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==csv_valueParser.T__0:
                    self.state = 29
                    self.match(csv_valueParser.T__0)
                    self.state = 30
                    self.value()
                    self.state = 35
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 36
                self.match(csv_valueParser.T__2)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 38
                self.match(csv_valueParser.LBRACK)
                self.state = 39
                self.match(csv_valueParser.T__2)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ValueContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return csv_valueParser.RULE_value

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class ObjectValueContext(ValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a csv_valueParser.ValueContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def obj(self):
            return self.getTypedRuleContext(csv_valueParser.ObjContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterObjectValue" ):
                listener.enterObjectValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitObjectValue" ):
                listener.exitObjectValue(self)


    class NullValueContext(ValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a csv_valueParser.ValueContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNullValue" ):
                listener.enterNullValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNullValue" ):
                listener.exitNullValue(self)


    class NumberValueContext(ValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a csv_valueParser.ValueContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NUMBER(self):
            return self.getToken(csv_valueParser.NUMBER, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumberValue" ):
                listener.enterNumberValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumberValue" ):
                listener.exitNumberValue(self)


    class BoolValueContext(ValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a csv_valueParser.ValueContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBoolValue" ):
                listener.enterBoolValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBoolValue" ):
                listener.exitBoolValue(self)


    class StringValueContext(ValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a csv_valueParser.ValueContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def STRING(self):
            return self.getToken(csv_valueParser.STRING, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStringValue" ):
                listener.enterStringValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStringValue" ):
                listener.exitStringValue(self)


    class ArrayValueContext(ValueContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a csv_valueParser.ValueContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def array(self):
            return self.getTypedRuleContext(csv_valueParser.ArrayContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterArrayValue" ):
                listener.enterArrayValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitArrayValue" ):
                listener.exitArrayValue(self)



    def value(self):

        localctx = csv_valueParser.ValueContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_value)
        try:
            self.state = 49
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [csv_valueParser.STRING]:
                localctx = csv_valueParser.StringValueContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 42
                self.match(csv_valueParser.STRING)
                pass
            elif token in [csv_valueParser.NUMBER]:
                localctx = csv_valueParser.NumberValueContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 43
                self.match(csv_valueParser.NUMBER)
                pass
            elif token in [csv_valueParser.LCURLY]:
                localctx = csv_valueParser.ObjectValueContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 44
                self.obj()
                pass
            elif token in [csv_valueParser.LBRACK]:
                localctx = csv_valueParser.ArrayValueContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 45
                self.array()
                pass
            elif token in [csv_valueParser.T__3]:
                localctx = csv_valueParser.BoolValueContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 46
                self.match(csv_valueParser.T__3)
                pass
            elif token in [csv_valueParser.T__4]:
                localctx = csv_valueParser.BoolValueContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 47
                self.match(csv_valueParser.T__4)
                pass
            elif token in [csv_valueParser.T__5]:
                localctx = csv_valueParser.NullValueContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 48
                self.match(csv_valueParser.T__5)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





