grammar csv_value;

csv_value:   obj
    |   array
    ;

obj
    :   '{' value (';' value)* '}'
    |   '{' '}'
    ;
	
array
    :   '[' value (';' value)* ']'
    |   '[' ']'
    ;

value
    :   STRING		# StringValue
    |   NUMBER		# NumberValue
    |   obj     	# ObjectValue
    |   array  		# ArrayValue
    |   'true'		# BoolValue
    |   'false'		# BoolValue
    |   'null'		# NullValue
    ;

LCURLY : '{' ;
LBRACK : '[' ;
STRING :  '"' (ESC | ~["\\])* '"' ;

fragment ESC :   '\\' (["\\/bfnrt] | UNICODE) ;
fragment UNICODE : 'u' HEX HEX HEX HEX ;
fragment HEX : [0-9a-fA-F] ;

NUMBER
    :   '-'? INT '.' DEC EXP?   // 1.35, 1.35E-9, 0.3, -4.5
    |   '-'? INT EXP            // 1e10 -3e4
    |   '-'? INT                // -3, 45
    ;
fragment INT :   '0' | '1'..'9' '0'..'9'* ; // no leading zeros
fragment DEC :   [0-9]*; // include leading zeros
fragment EXP :   [Ee] [+\-]? INT ; // \- since - means "range" inside [...]

WS  :   [ \t\n\r]+ -> skip ;
